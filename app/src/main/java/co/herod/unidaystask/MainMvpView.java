package co.herod.unidaystask;

import co.herod.unidaystask.mvp.MvpView;

/**
 * Created by Matthew Herod on 27/09/2016.
 */

public interface MainMvpView extends MvpView {

    void toastNothingToUpload();

    void toastProgress(int success, int failure);

    void toastError(String error);

}
