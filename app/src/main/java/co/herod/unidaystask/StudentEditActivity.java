package co.herod.unidaystask;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Matthew Herod on 25/09/2016.
 */

public class StudentEditActivity extends AppCompatActivity implements StudentEditMvpView {

    private StudentEditPresenter presenter;

    @BindView(R.id.editTextEmailWrapper)
    TextInputLayout textInputLayoutEmail;

    @BindView(R.id.editTextEmail)
    TextInputEditText editTextEmail;

    @BindView(R.id.editTextFirstNameWrapper)
    TextInputLayout textInputLayoutFirstName;

    @BindView(R.id.editTextFirstName)
    TextInputEditText editTextFirstName;

    @BindView(R.id.editTextLastNameWrapper)
    TextInputLayout textInputLayoutLastName;

    @BindView(R.id.editTextLastName)
    TextInputEditText editTextLastName;

    @BindView(R.id.radioButtonMale)
    RadioButton radioButtonMale;

    @BindView(R.id.radioButtonFemale)
    RadioButton radioButtonFemale;

    @BindView(R.id.radioButtonOther)
    RadioButton radioButtonOther;

    @BindView(R.id.universitySpinner)
    AppCompatSpinner universitySpinner;

    @BindView(R.id.submitButton)
    FloatingActionButton submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_edit);
        ButterKnife.bind(this);

        presenter = new StudentEditPresenter(this);
        presenter.attachView(this);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.universities, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        universitySpinner.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @OnClick(R.id.submitButton)
    void onClickSubmitButton() {
        String firstName = editTextFirstName.getText().toString();
        String lastName = editTextLastName.getText().toString();
        String email = editTextEmail.getText().toString().toLowerCase();
        String gender = getGenderSelection();
        String university = (String) universitySpinner.getSelectedItem();
        presenter.attemptSave(firstName, lastName, gender, email, university);
    }

    private String getGenderSelection() {
        if (radioButtonMale.isChecked())
            return radioButtonMale.getText().toString();
        if (radioButtonFemale.isChecked())
            return radioButtonFemale.getText().toString();
        // if (radioButtonOther.isChecked())
        return radioButtonOther.getText().toString();
    }

    @Override
    public void errorValidationEmail() {
        textInputLayoutEmail.setError(getString(R.string.hint_valid_email));
        textInputLayoutEmail.setErrorEnabled(true);
    }

    @Override
    public void resetErrorValidationEmail() {
        textInputLayoutEmail.setErrorEnabled(false);
    }

    @Override
    public void errorValidationFirstName() {
        textInputLayoutFirstName.setError(getString(R.string.hint_valid_first_name));
        textInputLayoutFirstName.setErrorEnabled(true);
    }

    @Override
    public void resetErrorValidationFirstName() {
        textInputLayoutFirstName.setErrorEnabled(false);
    }

    @Override
    public void errorValidationLastName() {
        textInputLayoutLastName.setError(getString(R.string.hint_valid_last_name));
        textInputLayoutLastName.setErrorEnabled(true);
    }

    @Override
    public void resetErrorValidationLastName() {
        textInputLayoutLastName.setErrorEnabled(false);
    }

    @Override
    public void successSave() {
        Toast.makeText(this, R.string.stored, Toast.LENGTH_SHORT).show();
        finish();
    }
}
