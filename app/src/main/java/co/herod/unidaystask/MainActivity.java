package co.herod.unidaystask;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Matthew Herod on 25/09/2016.
 */

public class MainActivity extends AppCompatActivity implements MainMvpView {

    private MainPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.btn_upload)
    Button uploadButton;

    @BindView(R.id.btn_create)
    Button createButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        presenter = new MainPresenter(this);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @OnClick(R.id.fab)
    void onClickFab(FloatingActionButton view) {
        presenter.attemptUpload();
    }

    @OnClick(R.id.btn_upload)
    void onClickUploadButton(Button view) {
        presenter.attemptUpload();
    }

    @OnClick(R.id.btn_create)
    void onClickCreateButton(Button view) {
        startActivity(new Intent(this, StudentEditActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void toastNothingToUpload() {
        Toast.makeText(this, "Nothing to upload", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toastProgress(int success, int failure) {
        Snackbar.make(toolbar, "Uploaded " + success + ", Failed " + failure, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void toastError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
