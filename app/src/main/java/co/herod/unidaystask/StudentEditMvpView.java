package co.herod.unidaystask;

import co.herod.unidaystask.mvp.MvpView;

/**
 * Created by Matthew Herod on 27/09/2016.
 */

public interface StudentEditMvpView extends MvpView {

    void errorValidationEmail();

    void resetErrorValidationEmail();

    void errorValidationFirstName();

    void resetErrorValidationFirstName();

    void errorValidationLastName();

    void resetErrorValidationLastName();

    void successSave();

}
