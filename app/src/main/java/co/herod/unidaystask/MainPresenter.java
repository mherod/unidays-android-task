package co.herod.unidaystask;

import android.content.Context;

import java.io.IOException;

import co.herod.unidaystask.mvp.BasePresenter;
import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Matthew Herod on 27/09/2016.
 */

public class MainPresenter extends BasePresenter<MainMvpView> {

    private UnidaysApiService unidaysApi;

    private Realm realm;

    private boolean uploading = false;

    private int failureCount = 0;

    private int successCount = 0;

    public MainPresenter(Context context) {
        super(context);
    }

    @Override
    public void attachView(MainMvpView mvpView) {
        super.attachView(mvpView);

        realm = Realm.getDefaultInstance();

        unidaysApi = UnidaysApi.service();
    }

    @Override
    public void detachView() {

        realm.close();

        super.detachView();
    }

    void attemptUpload() {

        final RealmResults<StudentRecord> studentRecords = realm
                .where(StudentRecord.class)
                .equalTo("uploaded", false)
                .findAll();

        if (studentRecords.size() == 0) {
            getMvpView().toastNothingToUpload();
            return;
        }

        if (studentRecords.size() > 0) { // && !uploading) {
            // uploading = true;
            successCount = 0;
            failureCount = 0;

            for (final StudentRecord s : studentRecords) {
                markStudentRecordUploadedAsync(realm.copyFromRealm(s));
            }
        }

    }

    private void markStudentRecordUploadedAsync(final StudentRecord studentRecord) {
        unidaysApi.upload(studentRecord).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    successCount++;

                    realm.beginTransaction();
                    studentRecord.setUploaded(true);
                    realm.copyToRealmOrUpdate(studentRecord);
                    realm.commitTransaction();

                    getMvpView().toastProgress(successCount, failureCount);
                } else {

                    failureCount++;

                    getMvpView().toastProgress(successCount, failureCount);

                    try {
                        String errorBody = response.errorBody().string();

                        Timber.e(errorBody);

                        getMvpView().toastError(errorBody);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                failureCount++;

                Timber.e(t);

                getMvpView().toastProgress(successCount, failureCount);

            }
        });
    }

}
