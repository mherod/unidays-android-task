package co.herod.unidaystask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Matthew Herod on 26/09/2016.
 */

public interface UnidaysApiService {

    @POST("xk8p92xk")
    Call<ResponseBody> upload(@Body StudentRecord studentRecord);

}
