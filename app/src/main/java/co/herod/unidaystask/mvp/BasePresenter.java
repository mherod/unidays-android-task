package co.herod.unidaystask.mvp;

/**
 * Created by Matthew Herod on 27/09/2016.
 */

import android.content.Context;

public class BasePresenter<T extends MvpView> implements Presenter<T> {

    private T mMvpView;
    private Context mContext;

    public BasePresenter(Context context) {
        mContext = context;
    }

    @Override
    public void attachView(T mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public T getMvpView() {
        return mMvpView;
    }

    public Context getContext() {
        return mContext;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.attachView(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }

}
