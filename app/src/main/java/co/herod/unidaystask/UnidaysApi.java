package co.herod.unidaystask;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Matthew Herod on 26/09/2016.
 */

public class UnidaysApi {

    public static UnidaysApiService service() {
        return new UnidaysApi().getService();
    }

    final private Retrofit retrofit;

    private UnidaysApi() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://requestb.in/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private UnidaysApiService getService() {
        return retrofit.create(UnidaysApiService.class);
    }

}
