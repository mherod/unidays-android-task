package co.herod.unidaystask;

import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;

import co.herod.unidaystask.mvp.BasePresenter;
import io.realm.Realm;
import timber.log.Timber;

/**
 * Created by Matthew Herod on 27/09/2016.
 */

public class StudentEditPresenter extends BasePresenter<StudentEditMvpView> {

    private Realm realm;

    // private final List<String> universityList;

    public StudentEditPresenter(Context context) {
        super(context);

        // String[] universities = getContext().getResources().getStringArray(R.array.universities);
        // universityList = Arrays.asList(universities);
    }

    @Override
    public void attachView(StudentEditMvpView mvpView) {
        super.attachView(mvpView);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void detachView() {
        realm.close();
        super.detachView();
    }

    void attemptSave(String firstName, String lastName, String gender, String email, String university) {

        boolean validationFailed = false;

        if (providedNameIsValid(firstName)) {
            getMvpView().resetErrorValidationFirstName();
        } else {
            getMvpView().errorValidationFirstName();
            validationFailed = true;
        }

        if (providedNameIsValid(lastName)) {
            getMvpView().resetErrorValidationLastName();
        } else {
            getMvpView().errorValidationLastName();
            validationFailed = true;
        }

        if (providedEmailIsValid(email)) {
            getMvpView().resetErrorValidationEmail();
        } else {
            getMvpView().errorValidationEmail();
            validationFailed = true;
        }

        /* if (universityList.contains(university)) {
        } */

        if (validationFailed) {
            // Go no further
            Timber.d("Failed validation");
            return;
        }

        Timber.d("Passed validation");

        final StudentRecord studentRecord = new StudentRecord();
        studentRecord.setEmailAddress(email);
        studentRecord.setFirstName(firstName);
        studentRecord.setLastName(lastName);
        studentRecord.setGender(gender);
        studentRecord.setUniversityName(university);

        saveStudentRecord(studentRecord);
    }

    private void saveStudentRecord(final StudentRecord studentRecord) {
        realm.beginTransaction();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                // With email as primary key we eliminate duplicates when OrUpdate
                bgRealm.copyToRealmOrUpdate(studentRecord);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                getMvpView().successSave();
            }
        });
        realm.commitTransaction();
    }

    private boolean providedEmailIsValid(CharSequence email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean providedNameIsValid(CharSequence name) {
        return !TextUtils.isEmpty(name) && name.length() >= 2;
    }

}
