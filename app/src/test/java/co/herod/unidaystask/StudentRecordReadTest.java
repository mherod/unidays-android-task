package co.herod.unidaystask;

import org.junit.Before;
import org.junit.Test;

import org.junit.Assert;

/**
 * Created by Matthew Herod on 26/09/2016.
 */
public class StudentRecordReadTest {

    private StudentRecord studentRecord;

    @Before
    public void setUp() throws Exception {
        studentRecord = new StudentRecord();
        studentRecord.setFirstName("Matthew");
        studentRecord.setLastName("Herod");
        studentRecord.setEmailAddress("matthew.herod@gmail.com");
    }

    @Test
    public void getFirstName() throws Exception {
        Assert.assertEquals(studentRecord.getFirstName(), "Matthew");
    }

    @Test
    public void getLastName() throws Exception {
        Assert.assertEquals(studentRecord.getLastName(), "Herod");
    }

    @Test
    public void getEmailAddress() throws Exception {
        Assert.assertEquals(studentRecord.getEmailAddress(), "matthew.herod@gmail.com");
    }

    @Test
    public void getGender() throws Exception {

    }

    @Test
    public void getUniversityName() throws Exception {

    }

}