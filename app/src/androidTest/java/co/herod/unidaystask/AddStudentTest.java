package co.herod.unidaystask;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AddStudentTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void addStudentTest() {
        ViewInteraction button = onView(
                allOf(withId(R.id.btn_upload),
                        childAtPosition(
                                allOf(withId(R.id.content_main),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
                                                1)),
                                0),
                        isDisplayed()));
        button.check(matches(isDisplayed()));

        ViewInteraction button2 = onView(
                allOf(withId(R.id.btn_create),
                        childAtPosition(
                                allOf(withId(R.id.content_main),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
                                                1)),
                                1),
                        isDisplayed()));
        button2.check(matches(isDisplayed()));

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btn_create), withText("Add Student"),
                        withParent(withId(R.id.content_main)),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction editText = onView(
                allOf(withId(R.id.editTextFirstName),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.editTextFirstNameWrapper),
                                        0),
                                0),
                        isDisplayed()));
        editText.check(matches(withText("")));

        ViewInteraction editText2 = onView(
                allOf(withId(R.id.editTextLastName),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.editTextLastNameWrapper),
                                        0),
                                0),
                        isDisplayed()));
        editText2.check(matches(withText("")));

        ViewInteraction radioButton = onView(
                allOf(withId(R.id.radioButtonMale),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.activity_student_edit),
                                        2),
                                0),
                        isDisplayed()));
        radioButton.check(matches(isDisplayed()));

        ViewInteraction radioButton2 = onView(
                allOf(withId(R.id.radioButtonFemale),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.activity_student_edit),
                                        2),
                                1),
                        isDisplayed()));
        radioButton2.check(matches(isDisplayed()));

        ViewInteraction radioButton3 = onView(
                allOf(withId(R.id.radioButtonOther),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.activity_student_edit),
                                        2),
                                2),
                        isDisplayed()));
        radioButton3.check(matches(isDisplayed()));

        ViewInteraction editText3 = onView(
                allOf(withId(R.id.editTextEmail),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.editTextEmailWrapper),
                                        0),
                                0),
                        isDisplayed()));
        editText3.check(matches(withText("")));

        ViewInteraction spinner = onView(
                allOf(withId(R.id.universitySpinner),
                        childAtPosition(
                                allOf(withId(R.id.activity_student_edit),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                4),
                        isDisplayed()));
        spinner.check(matches(isDisplayed()));

        ViewInteraction imageButton = onView(
                allOf(withId(R.id.submitButton),
                        childAtPosition(
                                allOf(withId(R.id.activity_student_edit),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                5),
                        isDisplayed()));
        imageButton.check(matches(isDisplayed()));

        ViewInteraction textInputEditText = onView(
                allOf(withId(R.id.editTextFirstName), isDisplayed()));
        textInputEditText.perform(click());

        ViewInteraction textInputEditText2 = onView(
                allOf(withId(R.id.editTextFirstName), isDisplayed()));
        textInputEditText2.perform(replaceText("Matthew"), closeSoftKeyboard());

        ViewInteraction textInputEditText3 = onView(
                allOf(withId(R.id.editTextLastName), isDisplayed()));
        textInputEditText3.perform(replaceText("Herod"), closeSoftKeyboard());

        ViewInteraction appCompatRadioButton = onView(
                allOf(withId(R.id.radioButtonMale), withText("Male"), isDisplayed()));
        appCompatRadioButton.perform(click());

        // pressBack();

        ViewInteraction textInputEditText4 = onView(
                allOf(withId(R.id.editTextEmail), isDisplayed()));
        textInputEditText4.perform(replaceText("matthew.herod@gmail.com"), closeSoftKeyboard());

        // pressBack();

        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.submitButton),
                        withParent(allOf(withId(R.id.activity_student_edit),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction button3 = onView(
                allOf(withId(R.id.btn_upload),
                        childAtPosition(
                                allOf(withId(R.id.content_main),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.view.ViewGroup.class),
                                                1)),
                                0),
                        isDisplayed()));
        button3.check(matches(isDisplayed()));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
