# Potential Improvements

We would like to know the next 3 iterations of the product, based on unlimited time etc.
In its current state - why was it made this way, what could be improved about it.
What features regardless of code and understanding would he like to see implemented. These need to be based in reality but can be things you don't know how to do.

# v1.0 ... it's current state

See README.md

## v1.0.1 ... belated MVP pattern refactor

## Idea backlog at this stage

* Rather than manually triggering the upload with button, intelligently attempt to upload in the background given appropriate hardware conditions, e.g. WiFi, good battery
* Rather than performing a HTTP post per student record, API specification depending, bundle pending student records into suitably sized bundles to reduce time spent opening connection
* Encrypt uploaded data, initially with HTTPS but also the model could have further encryption with a key specific to the user
* Beef up the user interface with UNiDAYS branding
* Ensure that student data entry form is as efficient as possible - e.g. hook into 'Next' and 'Done' keyboard operations.
* Automatically recognise university from email address
* Automatically recognise university based on device location (the closest campus which the device is to is a pretty good indicator)
* Facilitate more intuitive editing by pulling back records from local database into the form for editing and re-upload

# v1.1 ... next iteration

* thinking...

# v1.2 ... then the next

* thinking...

# v1.3 ... and another

* thinking...