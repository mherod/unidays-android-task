require 'calabash-android/calabash_steps'

Then /^I enter "([^\"]*)" as "([^\"]*)"$/ do |text, content_description|
    enter_text("* {contentDescription LIKE[c] '#{content_description}'}", text)
end

Then /^I enter "([^\"]*)" for "([^\"]*)"$/ do |text, content_description|
    clear_text_in("* {hint LIKE[c] '#{content_description}'}")
    enter_text("* {hint LIKE[c] '#{content_description}'}", text)
    hide_soft_keyboard
end

Then /^I touch the submit button$/ do
  hide_soft_keyboard
  sleep 1
  wait_for(:timeout => 5) { element_exists("* id:'submitButton'") }
  touch "* id:'submitButton'"
end
