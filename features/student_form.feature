Feature: Capture student details form, store and upload

  Scenario: As a user I should not find anything to upload at start
    Given I see the text "Upload"
    When I touch the "Upload" text
    Then I see the text "Nothing to upload"

  Scenario: As a user I can only provide a valid email address
    Given I see the text "Add Student"
    When I touch the "Add Student" text
    Then I don't see the text "Add Student"
    # Complete form
    Then I enter "notreal" for "Email"
    # Validation
    When I touch the submit button
    Then I see the text "Provide a valid email"
    # Complete form
    Then I enter "stupid@mail" for "Email"
    # Validation
    When I touch the submit button
    Then I see the text "Provide a valid email"
    # Complete form
    Then I enter "good@mail.com" for "Email"
    # Validation
    When I touch the submit button
    Then I don't see the text "Provide a valid email"
    # Complete form
    Then I enter "" for "Email"
    # Validation
    When I touch the submit button
    Then I see the text "Provide a valid email"

  Scenario: As a user I can open and complete the Create Student form
    Given I see the text "Add Student"
    When I touch the "Add Student" text
    Then I don't see the text "Add Student"
    # Complete form
    Then I enter "Adam" for "First name"
    Then I enter "Smith" for "Last name"
    Then I touch the "Male" text
    Then I touch the "University of" text
    Then I touch the "University of Leeds" text
    Then I enter "adam.smith@gmail.com" for "Email"
    When I touch the submit button
    Then I don't see the text "Provide a valid"
    Then I see the text "Stored"

  Scenario: As a user I can upload the stored record
    Given I see the text "Upload"
    When I touch the "Upload" text
    Then I see the text "Uploaded 1"
    Then I see the text "Failed 0"
    When I touch the "Upload" text
    Then I see the text "Nothing to upload"
