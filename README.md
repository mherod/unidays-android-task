# UNiDAYS Android Task
Submission by Matthew Herod

## Intro

This is my submission for my job application of the UNiDAYS Android Developer role.

I did send an email regarding what was meant by the UNiDAYS API but didn't get a response back before the weekend and so given that your deadline is Monday I am improvising by using [this requestb.in](http://requestb.in/xk8p92xk?inspect) as the API endpoint.

During development Git source control was used making conscious commits where small but substantial checkpoints were completed. Initially pushed to GitHub I added a second remote to Bitbucket so that I could share this project publicly without worrying about leaking this project so obviously.

I use libraries that handle what could have been tedious areas to implement if only using just SDK provided methods. Retrofit is used for HTTP API communication and Realm provides a powerful, simple to integrate database. Both of these have very minimal boilerplate, particularly in comparison with the Android SDK alternatives, which means less code to write and continually maintain (in the real world).

## Build

This Android project uses the Gradle build system; all build tasks can be found by running:

`./gradlew tasks`

To build the debug APK

`./gradlew assembleDebug`

To build the debug APK and install it on a connected device or emulator

`./gradlew installDebug`

## Testing

Testing of the app has not only been completed manually during development when debugging but also unit and instrumentation tests are written for automated testing.

To run all unit tests:

`./gradlew test`

To run all Espresso instrumentation tests:

`./gradlew connectedAndroidTest`

And to run Calabash Cucumber instrumentation tests:

(Aftering installing Gems with `bundle install`)

`bundle exec calabash-android run app/build/outputs/apk/app-debug.apk`

## Continuous Integration

CircleCI is building, running tests and reporting the success by setting the status of each GitHub commit. Cool!

This badge also shows the build status of the default branch:

[![CircleCI](https://circleci.com/gh/mherod/unidays-android-task.svg?style=svg&circle-token=9d44918b4690ff39f71db53c39361120dd6f0bad)](https://circleci.com/gh/mherod/unidays-android-task)

